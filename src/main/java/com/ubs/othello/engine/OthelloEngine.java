package com.ubs.othello.engine;

import com.ubs.othello.model.BoardCell;
import com.ubs.othello.model.Player;

import java.util.Optional;

/**
 * Represents the Othello rules:
 * 'X' goes first and must place an 'X' on the board, in such a position that there exists at least one straight
 * (horizontal, vertical, or diagonal) occupied line of 'O's between the new 'X' and another 'X' on the board.
 * After placing the piece, all 'O's lying on all straight lines between the new 'X' and any existing 'X' are captured
 * (i.e. they turn into 'X's )
 * Now 'O' plays. 'O' operates under the same rules, with the roles reversed: 'O' places an 'O' on the board in such a
 * position where at least one 'X' is captured
 *
 * @author Pierre
 */
public interface OthelloEngine {

    /**
     * Indicates if the game is completed, and no player can do another move. It can be either because the board
     * is full, or because no legal moves are available anymore.
     *
     * @return true if the game is finished.
     */
    boolean isFinished();

    /**
     * Indicates the winner of the game, at all time. The winner is the player with the most pieces on the board.
     *
     * @return A Player instance returning the winner or null if a tie.
     */
    Optional<Player> getWinner();

    /**
     * Play one move for the user. Silently pass the turn if the user cannot play.
     * After the move, all pieces between the new piece and any old piece on diagonals or same row / column
     * will be "captured".
     *
     * @param player    The Player trying to place a piece on the board
     * @param boardCell The board cell on which the player wants to place his piece.
     * @return true if the move was successful
     */
    boolean play(Player player, BoardCell boardCell);

    /**
     * Indicates if a specific player still has a chance to play.
     *
     * @param player The player to test for ability to play
     * @return true if the game can continue for this player.
     */
    boolean canPlay(Player player);
}
