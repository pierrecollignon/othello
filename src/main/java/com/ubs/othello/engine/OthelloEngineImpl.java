package com.ubs.othello.engine;

import com.ubs.othello.model.Board;
import com.ubs.othello.model.BoardCell;
import com.ubs.othello.model.BoardCellImpl;
import com.ubs.othello.model.Player;

import java.util.*;

/**
 * Implements an Othello rule engine following the rules specified in the
 * OthelloEngine interface.
 * <p>
 * We decouple the rules from the board model to provide extensibility.
 *
 * @author Pierre
 */
public class OthelloEngineImpl implements OthelloEngine {

    private final Board board;

    /**
     * Initialize the Othello rules engined with a board. The board
     * is in turn set in the initial state of the game. The board cannot
     * be changed mid-game and cannot be accessed from the consumer.
     *
     * @param board The board created by our consumer.
     */
    public OthelloEngineImpl(final Board board) {

        if (board == null) {
            throw new NullPointerException("The board cannot be null.");
        }

        this.board = board;
        // The board is initialized to the central position.
        board.setPiece("e4", Player.X);
        board.setPiece("d5", Player.X);

        board.setPiece("e5", Player.O);
        board.setPiece("d4", Player.O);
    }

    @Override
    public boolean isFinished() {

        if (board.isFilled()) {
            return true;
        }
        for (Player player : Player.values()) {
            if (canPlay(player)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Optional<Player> getWinner() {
        Map<Player, Integer> piecesPerPlayer = board.countPiecesPerPlayer();
        // We handle the special case of a tie
        boolean allEquals = Arrays.stream(Player.values()).map(piecesPerPlayer::get).distinct().limit(2).count() <= 1;
        if (allEquals) {
            return Optional.empty();
        }

        return Arrays.stream(Player.values()).max(Comparator.comparing(piecesPerPlayer::get));
    }

    /**
     * Generic board scanning algorithm which will find all cells on the board which can be captured for a specific
     * in a specific direction. The cells are capturable if they are in a continuous line between two pieces of the
     * same player. The scanning is generic, because it doesn't flip pieces, allowing it to be used for flipping or
     * possible play positions.
     *
     * @param board        The board to scan
     * @param startingCell The starting point of the scan
     * @param horizontal   The horizontal part of the scan vector (negative means jump step to the left, positive to the right)
     * @param vertical     The vertical part of the scan vector (negative means up and positive means left)
     * @param player       The player for which we want to find captureable cells.
     * @return A list of cells the player can capture in that direct, 0 if none.
     */
    private List<BoardCell> getCaptureablesInDirection(final Board board, final BoardCell startingCell,
                                                       final int horizontal, final int vertical, final Player player) {

        List<BoardCell> captureables = new ArrayList<>();
        boolean capturing = true;

        // We move one position toward the direction to not consider the cell where we want to be.
        int column = startingCell.getColumnIndex() + horizontal;
        int row = startingCell.getRowIndex() + vertical;

        while (column >= 0 && column < board.getSize() && row >= 0 && row < board.getSize() && capturing) {

            BoardCell currentCell = BoardCellImpl.getBoardCell(column, row);
            Player currentPlayer = board.getPiece(currentCell);
            if (currentPlayer == null) {
                // The chain is broken, we clear the captureable and stop the loop
                capturing = false;
                captureables.clear();
            } else if (currentPlayer == player) {
                // The capture is done
                capturing = false;
            } else {
                captureables.add(currentCell);
            }

            // We move one position toward the direction
            column += horizontal;
            row += vertical;
        }

        // The process couldn't finish: we should not keep any captureable.
        if (capturing) {
            captureables.clear();
        }
        return captureables;
    }

    private List<BoardCell> getAllCaptureables(final BoardCell startingCell, final Player player) {

        List<BoardCell> captureables = new LinkedList<>();
        for (int horizontal = -1; horizontal <= 1; horizontal++) {
            for (int vertical = -1; vertical <= 1; vertical++) {
                captureables.addAll(getCaptureablesInDirection(board, startingCell, horizontal, vertical, player));
            }
        }
        return captureables;
    }

    @Override
    public boolean canPlay(final Player player) {
        if (player == null) {
            throw new NullPointerException("The player is null.");
        }

        // A simple way to see if the user can still play is to test-run the capture
        // algo on each empty cell.

        List<BoardCell> emptyCells = board.getEmptyCells();
        for (BoardCell boardCell : emptyCells) {
            if (getAllCaptureables(boardCell, player).size() > 0) {
                return true;
            }
        }
        return false;


    }

    @Override
    public boolean play(final Player player, final BoardCell boardCell) {
        if (player == null) {
            throw new NullPointerException("The player is null.");
        }
        if (boardCell == null) {
            throw new NullPointerException("The cell is null.");
        }
        // Must ensure the cell is empty
        if (board.getPiece(boardCell) == null) {
            // We find all potential cells for capture
            List<BoardCell> captureables = getAllCaptureables(boardCell, player);
            if (captureables.size() > 0) {
                // We play
                board.setPiece(boardCell, player);
                // We capture
                captureables.forEach(cell -> board.setPiece(cell, player));
                return true;
            }
        }

        return false;
    }
}
