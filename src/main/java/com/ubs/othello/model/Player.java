package com.ubs.othello.model;

/**
 * Represents the Othello players available.
 *
 * @author Pierre
 */
public enum Player {
    X,
    O;

    /**
     * Returns the next Player available.
     *
     * @return Another player by order of priority.
     */
    public Player next() {
        return values()[(this.ordinal() + 1) % values().length];
    }
}
