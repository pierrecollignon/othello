package com.ubs.othello.model;

import java.util.List;
import java.util.Map;

/**
 * Represents an Othello board, of 8x8 cells, indexed by numbers vertically and letters horizontally.
 * The row and column can both be represented by a char.
 *
 * @author Pierre
 */
public interface Board {

    /**
     * Provides the size of the board. We constrain the board to be a square.
     *
     * @return The size of the board.
     */
    int getSize();

    /**
     * Finds the player piece on the board.
     *
     * @param cell The cell position of the piece.
     * @return A Player enum instance if a piece is found, null otherwise
     */
    Player getPiece(BoardCell cell);

    /**
     * Sets a piece on the board at the specified coordinates. Does not implement the rules and erase any piece
     * placed previously
     *
     * @param cell  The cell position of the piece.
     * @param piece The player piece to place on the board, or null to erase.
     */
    void setPiece(BoardCell cell, Player piece);

    /**
     * Sets a piece on the board at the specified coordinates. Does not implement the rules and erase any piece
     * placed previously
     *
     * @param cell  The string representation of the cell (i.e.: a1, 1a, 1A).
     * @param piece The piece to place on the board
     */
    void setPiece(String cell, Player piece);

    /**
     * Sets a piece on the board using a 0 to 7 coordinate system. (0,0) is the top left corner
     *
     * @param column The column index between 0 and 7.
     * @param row    The row index between 0 and 7.
     * @param piece  The piece to place on the board.
     */
    void setPiece(int column, int row, Player piece);

    /**
     * Indicates if the whole board is covered by pieces.
     *
     * @return true if the board is filled.
     */
    boolean isFilled();

    /**
     * Counts the number of pieces per player.
     */
    Map<Player, Integer> countPiecesPerPlayer();

    /**
     * Gets all empty cells on the board.
     *
     * @return All the empty cells
     */
    List<BoardCell> getEmptyCells();

}
