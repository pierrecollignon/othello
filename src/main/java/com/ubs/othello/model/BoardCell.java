package com.ubs.othello.model;

/**
 * Simple representation of a board cell, with a row and column
 *
 * @author Pierre
 */
public interface BoardCell {

    /**
     * Represents a row index between 0 and 7 from top to bottom.
     *
     * @return The integer representation of the row index.
     */
    int getRowIndex();

    /**
     * Represents a column index between 0 and 7 from left to right.
     *
     * @return The integer representation of the column index.
     */
    int getColumnIndex();
}
