package com.ubs.othello.model;

/**
 * Board cell implementation using a 2-letter String to represent its position.
 *
 * @author Pierre
 */
public class BoardCellImpl implements BoardCell {

    private final int column;
    private final int row;

    /**
     * A board cell can only be created via factory.
     *
     * @param column The column index between 0 and 7
     * @param row    The row index between 0 and 7
     */
    private BoardCellImpl(final int column, final int row) {
        this.column = column;
        this.row = row;
    }

    /**
     * A method factory creating the board cell.
     * Builds a board cell taking a full string representation. Can be either row+column or column+row, and is limited
     * to 2 characters.
     *
     * @param cellRepresentation The String cell representation
     * @return A BoardCell row / column if the cell representation is correct, null otherwise.
     */
    public static BoardCell getBoardCell(final String cellRepresentation) {
        if (cellRepresentation == null || cellRepresentation.length() != 2) {
            return null;
        }
        String internalRepresentation = cellRepresentation.toLowerCase();
        char firstChar = internalRepresentation.charAt(0);
        char secondChar = internalRepresentation.charAt(1);

        BoardCell boardCell = createBoardCell(firstChar, secondChar);
        if (boardCell == null) {
            boardCell = createBoardCell(secondChar, firstChar);
        }
        return boardCell; // Can return null if wrong representation.
    }

    /**
     * Creates a board cell by index of column and row, between 0 and 7.
     *
     * @param column The index of the column, between 0 and 7.
     * @param row    The index of the row, between 0 and 7.
     * @return A board cell representation of the coordinates.
     */
    public static BoardCell getBoardCell(final int column, final int row) {

        if (column < 0 || row < 0) {
            throw new IllegalArgumentException("Column and row must be on the board (" + column + ", " + row + ")");
        }

        return new BoardCellImpl(column, row);
    }

    private static BoardCell createBoardCell(final char column, final char row) {
        // We consider no constraint on the values, beyond the 2-letter code. The board cell
        // coordinate is not controlling the board size.
        if (column >= 'a' && column <= 'z' && row >= '1' && row <= '9') {
            return new BoardCellImpl(column - 'a', row - '1');
        }
        return null;
    }

    public int getRowIndex() {
        return row;
    }

    public int getColumnIndex() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BoardCellImpl boardCell = (BoardCellImpl) o;

        return column == boardCell.column && row == boardCell.row;
    }

    @Override
    public int hashCode() {
        int result = column;
        result = 31 * result + row;
        return result;
    }
}
