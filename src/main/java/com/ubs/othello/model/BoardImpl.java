package com.ubs.othello.model;

import org.apache.commons.lang3.mutable.MutableBoolean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implements an Othello board, with a two-dimensional piece array.
 *
 * @author Pierre
 */
public class BoardImpl implements Board {

    private static final int SIZE = 8;

    private final Player[][] board;

    public BoardImpl() {
        board = new Player[SIZE][SIZE];
        // Default init to null.
    }

    @Override
    public int getSize() {
        return SIZE;
    }

    @Override
    public Player getPiece(final BoardCell cell) {
        return board[cell.getColumnIndex()][cell.getRowIndex()];
    }

    @Override
    public void setPiece(final BoardCell cell, final Player piece) {
        setPiece(cell.getColumnIndex(), cell.getRowIndex(), piece);
    }

    @Override
    public void setPiece(final String cell, final Player piece) {
        BoardCell boardCell = BoardCellImpl.getBoardCell(cell);
        setPiece(boardCell, piece);
    }

    @Override
    public void setPiece(final int column, final int row, final Player piece) {
        if (row < 0 || row >= SIZE || column < 0 || column >= SIZE) {
            throw new IllegalArgumentException("Row and column should be on the board");
        }

        board[column][row] = piece;
    }

    @Override
    public boolean isFilled() {

        MutableBoolean mutableBoolean = new MutableBoolean(true);
        iterate((player, column, row) -> {
            if (player == null) {
                mutableBoolean.setFalse(); // This percolates the value on the top
                return false; // This breaks the loop
            }
            return true;
        });
        return mutableBoolean.booleanValue();
    }


    @Override
    public Map<Player, Integer> countPiecesPerPlayer() {
        Map<Player, Integer> piecesPerPlayer = new HashMap<>();
        for (Player player : Player.values()) {
            piecesPerPlayer.put(player, 0);
        }
        iterate((player, column, row) -> {
            if (player != null) {
                piecesPerPlayer.put(player, piecesPerPlayer.get(player) + 1);
            }
            return true;
        });

        return piecesPerPlayer;
    }

    @Override
    public List<BoardCell> getEmptyCells() {
        List<BoardCell> boardCells = new ArrayList<>();
        iterate((player, column, row) -> {
            if (player == null) {
                boardCells.add(BoardCellImpl.getBoardCell(column, row));
            }
            return true;
        });

        return boardCells;
    }

    /**
     * A lot of the code is a repetition of this iteration. It can be simplified
     * and provided a lambda Consumer having three parameters.
     *
     * @param f The function to call on each cell of the board, returns a boolean
     *          to stop the flow.
     */
    private void iterate(final BoardIterationFunction f) {
        for (int column = 0; column < SIZE; column++) {
            for (int row = 0; row < SIZE; row++) {
                if (!f.apply(board[column][row], column, row)) {
                    return;
                }
            }
        }
    }

    /**
     * Represents a function that can be run on each cell of the board,
     * using the player, column and row as input.
     */
    @FunctionalInterface
    private interface BoardIterationFunction {
        boolean apply(final Player player, final int column, final int row);
    }

}
