package com.ubs.othello.ui;

import com.ubs.othello.model.Player;

/**
 * Displays to the user the state of the board and sends various message. Not assumption
 * is made on how this is implemented.
 *
 * @author Pierre
 */
public interface BoardDisplay {

    /**
     * A multi-line string representation of the board, for testing and debugging purposes. Can also
     * be used as the main display interface.
     *
     * @return A string representation of the whole board, following the examples provided.
     */
    String getDisplay();

    /**
     * Display a warning if the user cannot play a turn.
     *
     * @param currentPlayer The player who cannot play the turn.
     */
    void displayCannotPlayTurn(Player currentPlayer);

    /**
     * Displays a warning if the move is invalid, meaning either the command was not recognized or the move cannot be
     * made following the rules of the game being played.
     */
    void displayInvalidMove();

    /**
     * Display the command handle to the user, to ask for input.
     *
     * @param currentPlayer The player whose turn it is to give a command.
     */
    void displayHandle(Player currentPlayer);

    /**
     * Tells the players that the game is finished.
     */
    void displayEndOfGame();

    /**
     * Display informations concerning a tie (same score for all players).
     *
     * @param score The score shared by all players.
     */
    void displayTie(int score);

    /**
     * Shows the winner of the game
     *
     * @param winner      The player who won the came
     * @param winnerScore The winner score
     * @param loserScore  The loser score
     */
    void displayWinner(Player winner, int winnerScore, int loserScore);
}
