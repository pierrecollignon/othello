package com.ubs.othello.ui;

import com.ubs.othello.model.Board;
import com.ubs.othello.model.BoardCellImpl;
import com.ubs.othello.model.Player;

/**
 * Standard display implementation, to mimic what was provided in the README.
 *
 * @author Pierre
 */
public class BoardDisplayImpl implements BoardDisplay {

    private final Board board;

    public BoardDisplayImpl(Board board) {
        this.board = board;
    }

    @Override
    public String getDisplay() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int line = 0; line < board.getSize(); line++) {
            stringBuilder.append(line + 1).append(" ");
            for (int column = 0; column < board.getSize(); column++) {
                Player player = board.getPiece(BoardCellImpl.getBoardCell(column, line));
                if (player != null) {
                    stringBuilder.append(player.name());
                } else {
                    stringBuilder.append("-");
                }
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("  ");
        for (int column = 0; column < board.getSize(); column++) {
            stringBuilder.append((char) ('a' + column));
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    /**
     * Main function which will choose how to display on the screen. Can be muted or changed by
     * inheritance.
     *
     * @param message The message to display
     */
    protected void display(String message) {

        System.out.println(message);
        System.out.flush();
    }

    @Override
    public void displayCannotPlayTurn(Player currentPlayer) {

        display("Sorry Player '" + currentPlayer + "' cannot play this turn !");
    }

    @Override
    public void displayInvalidMove() {

        display("Invalid move. Please try again.\n");
    }

    @Override
    public void displayHandle(Player currentPlayer) {

        display(getDisplay());
        display("Player '" + currentPlayer + "' move: ");
    }

    @Override
    public void displayEndOfGame() {

        display("No further moves available");
    }

    @Override
    public void displayTie(int score) {

        display("This is a tie (" + score + ")");
    }

    @Override
    public void displayWinner(Player winner, int winnerScore, int loserScore) {

        display(
                "Player '" + winner + "' wins (" + winnerScore
                        + " vs "
                        + loserScore
                        + ")"
        );

        display(getDisplay());
    }
}
