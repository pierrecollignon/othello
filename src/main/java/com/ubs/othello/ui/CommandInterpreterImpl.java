package com.ubs.othello.ui;

import com.ubs.othello.model.BoardCell;
import com.ubs.othello.model.BoardCellImpl;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Implements a very simple Command Interpreted, dedicated to the Othello board positioning.
 *
 * @author Pierre
 */
public class CommandInterpreterImpl implements CommandInterpreter<BoardCell> {

    private final Scanner scanner;

    public CommandInterpreterImpl() {
        scanner = new Scanner(System.in, StandardCharsets.UTF_8.name());
    }

    /**
     * Returns a cell on a board.
     *
     * @return The cell the user want to represent, null if not a cell.
     */
    @Override
    public BoardCell getInput() {

        String boardCellInput = scanner.nextLine();
        return BoardCellImpl.getBoardCell(boardCellInput);
    }


}
