package com.ubs.othello.ui;

/**
 * Interprets command received from an input source.
 *
 * @param <T> The type of command that can be received.
 * @author Pierre
 */
public interface CommandInterpreter<T> {

    /**
     * Waits for a user input and returns a command type.
     *
     * @return A command data type representing the user input.
     */
    T getInput();

}
