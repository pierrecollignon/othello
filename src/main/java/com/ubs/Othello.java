package com.ubs;

import com.ubs.othello.engine.OthelloEngine;
import com.ubs.othello.engine.OthelloEngineImpl;
import com.ubs.othello.model.Board;
import com.ubs.othello.model.BoardCell;
import com.ubs.othello.model.BoardImpl;
import com.ubs.othello.model.Player;
import com.ubs.othello.ui.BoardDisplay;
import com.ubs.othello.ui.BoardDisplayImpl;
import com.ubs.othello.ui.CommandInterpreter;
import com.ubs.othello.ui.CommandInterpreterImpl;

import java.util.Map;

/**
 * Main Othello application. Implements the following play logic:
 * 1. Starts with player X
 * 2. Asks the user for input at each turn
 * 3. Play the move the user wanted
 * 4. Display the table
 * 5. Checks if the game is finished and display the winner if so.
 *
 * @author Pierre
 */
class Othello {

    private final Board board;
    private final OthelloEngine engine;
    private final CommandInterpreter<BoardCell> commandInterpreter;
    private final BoardDisplay boardDisplay;

    /**
     * Builds an Othello game runner, injecting all dependencies via constructor
     *
     * @param board              The board to use
     * @param othelloEngine      The engine to use
     * @param commandInterpreter The input method to use
     * @param boardDisplay       The display driver to use
     */
    Othello(final Board board, final OthelloEngine othelloEngine,
            final CommandInterpreter<BoardCell> commandInterpreter, final BoardDisplay boardDisplay) {
        this.board = board;
        this.engine = othelloEngine;
        this.commandInterpreter = commandInterpreter;
        this.boardDisplay = boardDisplay;
    }

    // This is not covered by unit tests, as it is trivial.
    public static void main(String[] args) {
        Board board = new BoardImpl();
        new Othello(board, new OthelloEngineImpl(board), new CommandInterpreterImpl(), new BoardDisplayImpl(board)).runGame();
    }

    OthelloEngine getEngine() {
        return engine;
    }

    CommandInterpreter getCommandInterpreter() {
        return commandInterpreter;
    }

    BoardDisplay getBoardDisplay() {
        return boardDisplay;
    }

    /**
     * Runs the game, by listening on the keyboard for input.
     * <p>
     * The loop can be broken by putting 'es' as input.
     */
    void runGame() {
        Player currentPlayer = Player.X;
        while (!engine.isFinished()) {
            if (!engine.canPlay(currentPlayer)) {
                boardDisplay.displayCannotPlayTurn(currentPlayer);
                currentPlayer = currentPlayer.next();
            } else {
                boardDisplay.displayHandle(currentPlayer);
                BoardCell boardCell = commandInterpreter.getInput();
                if (boardCell == null) {
                    boardDisplay.displayInvalidMove();
                } else {
                    boolean valid = engine.play(currentPlayer, boardCell);
                    if (!valid) {
                        boardDisplay.displayInvalidMove();
                    } else {
                        currentPlayer = currentPlayer.next();
                    }
                }
            }
        }

        boardDisplay.displayEndOfGame();

        Map<Player, Integer> piecesPerPlayer = board.countPiecesPerPlayer();
        if (!engine.getWinner().isPresent()) {
            boardDisplay.displayTie(piecesPerPlayer.get(Player.values()[0]));
        } else {
            Player winner = engine.getWinner().get();
            boardDisplay.displayWinner(winner, piecesPerPlayer.get(winner), piecesPerPlayer.get(winner.next()));
        }

    }
}
