package com.ubs.othello.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Map;

import static com.ubs.othello.model.Player.X;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;

public class BoardTest {

    private final static BoardCell BOARD_CELL = BoardCellImpl.getBoardCell("a1");
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void canEraseAPiece() {
        Board board = new BoardImpl();
        board.setPiece(BOARD_CELL, X);
        board.setPiece(BOARD_CELL, null);

        assertNull(board.getPiece(BOARD_CELL));
    }

    @Test
    public void canSetAO() {
        Board board = new BoardImpl();
        board.setPiece(BOARD_CELL, Player.O);

        assertEquals(Player.O, board.getPiece(BOARD_CELL));
    }

    @Test
    public void canSetAnX() {
        Board board = new BoardImpl();
        board.setPiece(BOARD_CELL, X);

        assertEquals(X, board.getPiece(BOARD_CELL));
    }

    private void fillBoardCell(Board board, int column, int row) {
        BoardCell boardCell = BoardCellImpl.getBoardCell(column, row);
        board.setPiece(boardCell, X);
    }

    @Test
    public void isFilledIfAllAreNonNull() {
        Board board = new BoardImpl();
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                fillBoardCell(board, i, j);
            }
        }
        assertTrue(board.isFilled());
    }

    @Test
    public void isNotFilledIfAtLeast1CellIsNull() {
        Board board = new BoardImpl();
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                fillBoardCell(board, i, j);
            }
        }
        board.setPiece(BoardCellImpl.getBoardCell("a1"), null);
        assertFalse(board.isFilled());
    }

    @Test
    public void countPiecesPerPlayer() {
        Board board = new BoardImpl();
        board.setPiece(BoardCellImpl.getBoardCell("a1"), X);
        board.setPiece(BoardCellImpl.getBoardCell("b1"), X);
        board.setPiece(BoardCellImpl.getBoardCell("a2"), X);


        board.setPiece(BoardCellImpl.getBoardCell("a3"), Player.O);
        board.setPiece(BoardCellImpl.getBoardCell("b2"), Player.O);

        Map<Player, Integer> piecesPerPlayer = board.countPiecesPerPlayer();
        assertEquals((Integer) 3, piecesPerPlayer.get(Player.X));
        assertEquals((Integer) 2, piecesPerPlayer.get(Player.O));
    }

    @Test
    public void rowShouldNotBeLeftOfTheBoard() {

        thrown.expect(IllegalArgumentException.class);
        Board board = new BoardImpl();
        board.setPiece(0, -1, null);
    }

    @Test
    public void rowShouldNotBeRightOfTheBoard() {

        thrown.expect(IllegalArgumentException.class);
        Board board = new BoardImpl();
        board.setPiece(0, board.getSize(), null);
    }

    @Test
    public void columnShouldNotBeTopOfTheBoard() {

        thrown.expect(IllegalArgumentException.class);
        Board board = new BoardImpl();
        board.setPiece(-1, 0, null);
    }

    @Test
    public void columnShouldNotBeBottomOfTheBoard() {

        thrown.expect(IllegalArgumentException.class);
        Board board = new BoardImpl();
        board.setPiece(board.getSize() + 1, 0, null);
    }


}
