package com.ubs.othello.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class BoardCellTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void canGetCellFromRowColumnRepresentation() {
        BoardCell boardCell = BoardCellImpl.getBoardCell("1a");

        assertEquals(0, boardCell.getRowIndex());
        assertEquals(0, boardCell.getColumnIndex());
    }

    @Test
    public void canGetCellFromColumnRowRepresentation() {
        BoardCell boardCell = BoardCellImpl.getBoardCell("e6");

        assertEquals(5, boardCell.getRowIndex());
        assertEquals(4, boardCell.getColumnIndex());
    }

    @Test
    public void boardCellIsNullIfInvalidRepresentation() {
        BoardCell boardCell = BoardCellImpl.getBoardCell("invalid");
        assertNull(boardCell);
    }

    @Test
    public void allLegalCombinationsAreNotNull() {
        for (int row = 0; row < 8; row++) {
            for (char column = 'a'; column <= 'h'; column++) {
                assertNotNull(BoardCellImpl.getBoardCell(column + Integer.toString(row + 1)));
            }
        }
    }

    @Test
    public void canGetCellFromNumericCoordinates() {
        BoardCell boardCell = BoardCellImpl.getBoardCell(1, 1);

        assertEquals(1, boardCell.getRowIndex());
        assertEquals(1, boardCell.getColumnIndex());
    }

    @Test
    public void cellsAreEqualIfSamePosition() {
        BoardCell boardCell1 = BoardCellImpl.getBoardCell("a3");
        BoardCell boardCell2 = BoardCellImpl.getBoardCell("a3");

        assertEquals(boardCell1, boardCell2);
    }

    @Test
    public void hashCodeAndEqualAreCoherentIfEqual() {
        BoardCell boardCell1 = BoardCellImpl.getBoardCell("a3");
        BoardCell boardCell2 = BoardCellImpl.getBoardCell("a3");

        assertEquals(boardCell1.hashCode(), boardCell2.hashCode());
    }

    @Test
    public void columnShouldNotBeUnder0() {

        thrown.expect(IllegalArgumentException.class);
        BoardCellImpl.getBoardCell(-1, 0);
    }

    @Test
    public void rowShouldNotBeUnder0() {

        thrown.expect(IllegalArgumentException.class);
        BoardCellImpl.getBoardCell(1, -1);
    }
}
