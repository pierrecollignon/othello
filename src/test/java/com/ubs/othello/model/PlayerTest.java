package com.ubs.othello.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlayerTest {

    @Test
    public void nextReturnsXIfCurrentO() {
        Player player = Player.O;
        assertEquals(Player.X, player.next());
    }

    @Test
    public void nextReturnsOIfCurrentX() {
        Player player = Player.X;
        assertEquals(Player.O, player.next());
    }
}
