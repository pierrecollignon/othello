package com.ubs.othello.ui;

import com.ubs.othello.model.BoardCellImpl;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * The scanner cannot be mocked, but the System.in can be set.
 */
public class CommandInterpreterTest {

    @Test
    public void nullIfNotACell() throws UnsupportedEncodingException {
        System.setIn(new ByteArrayInputStream("Invalid\n".getBytes(StandardCharsets.UTF_8.name())));

        CommandInterpreterImpl keyInput = new CommandInterpreterImpl();

        assertNull(keyInput.getInput());
    }

    @Test
    public void transformCellReferenceCorrectly() throws UnsupportedEncodingException {
        System.setIn(new ByteArrayInputStream("a1\n".getBytes(StandardCharsets.UTF_8.name())));

        CommandInterpreterImpl keyInput = new CommandInterpreterImpl();
        assertEquals(BoardCellImpl.getBoardCell("a1"), keyInput.getInput());
    }

}
