package com.ubs.othello.ui;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.ubs.othello.engine.OthelloEngineImpl;
import com.ubs.othello.model.Board;
import com.ubs.othello.model.BoardCellImpl;
import com.ubs.othello.model.BoardImpl;
import com.ubs.othello.model.Player;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * The readme steps are used to validate the display is correct.
 */
public class BoardDisplayTest {

    @Test
    public void displayAllX() throws IOException {
        Board board = new BoardImpl();
        for (int line = 0; line < board.getSize(); line++) {
            for (int column = 0; column < board.getSize(); column++) {
                board.setPiece(BoardCellImpl.getBoardCell(column, line), Player.X);
            }
        }

        BoardDisplay boardDisplay = new BoardDisplayImpl(board);

        String expected = Resources.toString(Resources.getResource("othello/ui/boards/allX.txt"), Charsets.UTF_8);

        assertEquals(expected, boardDisplay.getDisplay());
    }

    @Test
    public void displayAllO() throws IOException {
        Board board = new BoardImpl();
        for (int line = 0; line < board.getSize(); line++) {
            for (int column = 0; column < board.getSize(); column++) {
                board.setPiece(BoardCellImpl.getBoardCell(column, line), Player.O);
            }
        }

        BoardDisplay boardDisplay = new BoardDisplayImpl(board);

        String expected = Resources.toString(Resources.getResource("othello/ui/boards/allO.txt"), Charsets.UTF_8);

        assertEquals(expected, boardDisplay.getDisplay());

    }

    @Test
    public void displayInitState() throws IOException {
        Board board = new BoardImpl();
        new OthelloEngineImpl(board);


        BoardDisplay boardDisplay = new BoardDisplayImpl(board);

        String expected = Resources.toString(Resources.getResource("othello/ui/boards/initState.txt"), Charsets.UTF_8);

        assertEquals(expected, boardDisplay.getDisplay());
    }

}
