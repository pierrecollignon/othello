package com.ubs.othello.engine;

import com.ubs.othello.model.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OthelloEngineTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    private Board board;
    @Mock
    private BoardCell boardCell;

    @Test
    public void gameIsFinishedIfAllPiecesAreFilled() {
        when(board.isFilled()).thenReturn(true);

        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(board);
        assertTrue(othelloEngine.isFinished());
    }

    @Test
    public void playerCannotPlayOnFilledCell() {
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);
        when(board.getPiece(boardCell)).thenReturn(Player.X);
        assertFalse(othelloEngine.play(Player.X, boardCell));
    }

    private Board getRealBoardWithPlayer(String piece, Player player) {
        Board board = new BoardImpl();
        board.setPiece(BoardCellImpl.getBoardCell(piece), player);
        return board;
    }

    @Test
    public void playerCanPlayIfExistingPieceDiagLeft() {
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(getRealBoardWithPlayer("f6", Player.X));

        assertTrue(othelloEngine.play(
                Player.X,
                BoardCellImpl.getBoardCell("c3"))
        );
    }

    @Test
    public void playerSetCurrentCellToHimself() {
        Board board = getRealBoardWithPlayer("f6", Player.X);
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(board);
        BoardCell c3 = BoardCellImpl.getBoardCell("c3");
        othelloEngine.play(
                Player.X,
                c3);

        assertEquals(Player.X, board.getPiece(c3));
    }

    @Test
    public void playerCanPlayIfExistingPieceDiagRight() {
        Board board = new BoardImpl();
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(board);

        board.setPiece(BoardCellImpl.getBoardCell("c3"), Player.X);
        assertTrue(othelloEngine.play(
                Player.X,
                BoardCellImpl.getBoardCell("f6"))
        );
    }

    @Test
    public void playerCanPlayIfTouchesExistingPieceSameColumn() {
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(new BoardImpl());

        assertTrue(othelloEngine.play(
                Player.X,
                BoardCellImpl.getBoardCell("d3"))
        );
    }

    @Test
    public void playerCanPlayIfExistingPieceSameColumn() {
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(new BoardImpl());

        assertTrue(othelloEngine.play(
                Player.X,
                BoardCellImpl.getBoardCell("e6"))
        );
    }

    @Test
    public void playerCanPlayIfTouchesExistingPieceSameRow() {
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(new BoardImpl());

        assertTrue(othelloEngine.play(
                Player.X,
                BoardCellImpl.getBoardCell("f5"))
        );
    }

    @Test
    public void playerCanPlayIfExistingPieceSameRow() {
        OthelloEngineImpl othelloEngine = new OthelloEngineImpl(getRealBoardWithPlayer("a1", Player.X));

        assertTrue(othelloEngine.play(
                Player.X,
                BoardCellImpl.getBoardCell("f5"))
        );
    }

    @Test
    public void playerWinsIfMostPieces() {
        Map<Player, Integer> piecesPerPlayer = new HashMap<>();
        piecesPerPlayer.put(Player.X, 1);
        piecesPerPlayer.put(Player.O, 0);
        when(board.countPiecesPerPlayer()).thenReturn(piecesPerPlayer);

        OthelloEngine othelloEngine = new OthelloEngineImpl(board);

        assertEquals(Player.X, othelloEngine.getWinner().get());
    }

    @Test
    public void playersAreInTieIfSameAmountOfPieces() {
        Board board = new BoardImpl();
        // We artificially fill half the board with X and the other half with O
        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 8; column++) {
                board.setPiece(column, row, row < 4 ? Player.O : Player.X);
            }
        }

        OthelloEngine othelloEngine = new OthelloEngineImpl(board);
        assertFalse(othelloEngine.getWinner().isPresent());
    }

    /**
     * Init a finished board
     */
    private void initVlasakovaSchotte(Board board) {
        for (int row = 0; row < board.getSize(); row++) {
            for (int column = 0; column < board.getSize(); column++) {
                BoardCell boardCell = BoardCellImpl.getBoardCell(column, row);
                board.setPiece(boardCell, Player.X);
            }
        }

        board.setPiece("h4", null);
        board.setPiece("g5", null);
        board.setPiece("h5", null);
        board.setPiece("g6", null);
        board.setPiece("h7", null);

        board.setPiece("h6", Player.O);
    }

    @Test
    public void vlasakovaSchotteLastStateIsFinished() {
        // We use a classical case to test our engined stops the game the same way
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);

        initVlasakovaSchotte(board);

        assertTrue(othelloEngine.isFinished());
    }

    @Test
    public void vlasakovaSchotteLastStateIsCorrectWinner() {
        // We use a classical case to test our engine stops the game the same way
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);

        initVlasakovaSchotte(board);

        assertEquals(Player.X, othelloEngine.getWinner().get());
    }

    @Test
    public void playerXCanPlayFirstShot() {
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);

        BoardCell e6 = BoardCellImpl.getBoardCell("e6");

        assertTrue(othelloEngine.play(Player.X, e6));
    }

    @Test
    public void playersCanPlaySuccessively() {
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);

        BoardCell e6 = BoardCellImpl.getBoardCell("e6");
        BoardCell f4 = BoardCellImpl.getBoardCell("f4");
        assertTrue(othelloEngine.play(Player.X, e6));
        assertTrue(othelloEngine.play(Player.O, f4));
    }

    @Test
    public void boardIsInitializedCorrectly() {
        Board board = new BoardImpl();
        new OthelloEngineImpl(board);

        BoardCell d4 = BoardCellImpl.getBoardCell("d4");
        BoardCell d5 = BoardCellImpl.getBoardCell("d5");
        BoardCell e4 = BoardCellImpl.getBoardCell("e4");
        BoardCell e5 = BoardCellImpl.getBoardCell("e5");

        assertEquals(Player.X, board.getPiece(e4));
        assertEquals(Player.X, board.getPiece(d5));

        assertEquals(Player.O, board.getPiece(e5));
        assertEquals(Player.O, board.getPiece(d4));

    }

    @Test
    public void gameIsNotFinishedInInitState() {
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);

        assertFalse(othelloEngine.isFinished());
    }

    @Test
    public void playerShouldNotBeNullWhenPlaying() {
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);
        thrown.expect(NullPointerException.class);

        othelloEngine.play(null, boardCell);
    }

    @Test
    public void cellShouldNotBeNullWhenPlaying() {
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);
        thrown.expect(NullPointerException.class);

        othelloEngine.play(Player.O, null);
    }

    @Test
    public void playerShouldNotBeNullWhenTestingForPlayability() {
        Board board = new BoardImpl();
        OthelloEngine othelloEngine = new OthelloEngineImpl(board);
        thrown.expect(NullPointerException.class);

        othelloEngine.canPlay(null);
    }

    @Test
    public void boardShouldNotBeNullWhenCreatingEngine() {
        thrown.expect(NullPointerException.class);

        OthelloEngine othelloEngine = new OthelloEngineImpl(null);

    }


}
