package com.ubs;

import com.google.common.base.Splitter;
import com.ubs.othello.engine.OthelloEngine;
import com.ubs.othello.engine.OthelloEngineImpl;
import com.ubs.othello.model.*;
import com.ubs.othello.ui.BoardDisplay;
import com.ubs.othello.ui.BoardDisplayImpl;
import com.ubs.othello.ui.CommandInterpreter;
import com.ubs.othello.ui.CommandInterpreterImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * End to end tests of public games, to validate the business rules.
 * It is surprisingly hard to find existing transcripts of championships, and
 * a real-world scenario would include importing existing db formats of such championships
 * and do an exhaustive run on them.
 * <p>
 * The end to end tests will display on the System output stream, but a good regression test could
 * also import screen dumps (text) of the complete runs and validate against them the UI didn't change and
 * the board is indeed what it should be.
 */
@RunWith(MockitoJUnitRunner.class)
public class OthelloTest {

    @Test
    public void setupMustInitUI() {

        Board board = new BoardImpl();
        Othello othello = new Othello(board, new OthelloEngineImpl(board), new CommandInterpreterImpl(), new BoardDisplayImpl(board));

        assertNotNull(othello.getEngine());
        assertNotNull(othello.getBoardDisplay());
        assertNotNull(othello.getCommandInterpreter());
    }

    /**
     * This returns a mock of CommandInterpreter which will stream a sequence of moves.
     *
     * @param moves The moves to simulate in a 2char successive format
     * @return A keyInput object which will distribute the moves one by one
     */
    private CommandInterpreter<BoardCell> mockFullGameKeyInput(String moves) {
        Queue<String> moveQueue = new LinkedList<>();
        Splitter.fixedLength(2).split(moves).forEach(moveQueue::add);

        CommandInterpreter<BoardCell> commandInterpreter = mock(CommandInterpreterImpl.class);
        when(commandInterpreter.getInput()).then((Answer<BoardCell>) invocationOnMock -> {
                    String command = moveQueue.poll();
                    return BoardCellImpl.getBoardCell(command);
                }
        );

        return commandInterpreter;
    }

    /**
     * Custom assertion, to verify a score. Too complex, test many things at once. But convenient for batch test.
     */
    private void assertWinner(Player expectedWinner, int expectedWinnerPieces, int expectedLoserPieces,
                              OthelloEngine engine, Board board) {
        Map<Player, Integer> piecesPerPlayer = board.countPiecesPerPlayer();

        Optional<Player> winner = engine.getWinner();
        if (winner.isPresent()) {
            assertEquals(expectedWinner, winner.get());
            assertEquals(expectedWinnerPieces, (int) piecesPerPlayer.get(winner.get()));
            assertEquals(expectedLoserPieces, (int) piecesPerPlayer.get(winner.get().next()));
        } else {
            assertEquals(expectedWinnerPieces, (int) piecesPerPlayer.get(Player.O));
            assertEquals(expectedLoserPieces, (int) piecesPerPlayer.get(Player.X));
        }

    }

    private BoardDisplay getMutedBoardDisplaySpy(Board board) {
        // We mute the output, better would be to redirect and assert.
        return spy(new BoardDisplayImpl(board) {
            @Override
            public void display(String message) {
                // Do nothing
            }
        });
    }

    private void testKnowGame(String moves, Player expectedWinner, int expectedWinnerScore, int expectedLoserScore) {
        CommandInterpreter<BoardCell> mockInput = mockFullGameKeyInput(moves);
        Board board = new BoardImpl();


        Othello othello = new Othello(board, new OthelloEngineImpl(board), mockInput, getMutedBoardDisplaySpy(board));
        othello.runGame();

        assertWinner(expectedWinner, expectedWinnerScore, expectedLoserScore,
                othello.getEngine(),
                board
        );
    }

    @Test
    public void invalidCommandShouldDisplayError() {
        Board board = new BoardImpl();
        BoardDisplay boardDisplaySpy = getMutedBoardDisplaySpy(board);

        Othello othello = new Othello(
                board,
                new OthelloEngineImpl(board),
                // We insert three invalid input ('in') to simulate a user error
                mockFullGameKeyInput("inc4e3f6e6f5c5c3b4d3f4c6g4g5d6b5c2d2b3f3a6f7e1e7e2f2h6h3h5d1f1g1c1inb1b6a5a4g3e8f8d8c8ing6h4d7h7g2a2a3a7g7h1h2b7c7b8a8b2a1g8h8"),
                boardDisplaySpy);
        othello.runGame();

        verify(boardDisplaySpy, times(3)).displayInvalidMove();
    }

    /**
     * This test if the command is valid but the move is invalid.
     */
    @Test
    public void invalidPieceShouldDisplayError() {
        Board board = new BoardImpl();
        BoardDisplay boardDisplaySpy = getMutedBoardDisplaySpy(board);

        Othello othello = new Othello(
                board,
                new OthelloEngineImpl(board),
                // We insert three invalid input ('in') to simulate a user error
                mockFullGameKeyInput("c5c4e3f6e6f5c5c3b4d3f4c6g4g5d6b5c2d2b3f3a6f7e1e7e2f2h6h3h5d1f1g1c1b1b6a5a4g3e8f8d8c8g6h4d7h7g2a2a3a7g7h1h2b7c7b8a8b2a1g8h8"),
                boardDisplaySpy);
        othello.runGame();

        verify(boardDisplaySpy, times(1)).displayInvalidMove();
    }

    // http://www.o-wc.com/live/t14/p1.html
    @Test
    public void tamenoriVsHand() {
        testKnowGame("c4e3f6e6f5c5c3b4d3f4c6g4g5d6b5c2d2b3f3a6f7e1e7e2f2h6h3h5d1f1g1c1b1b6a5a4g3e8f8d8c8g6h4d7h7g2a2a3a7g7h1h2b7c7b8a8b2a1g8h8",
                Player.O,
                37,
                27);
    }

    // http://emmettnicholas.com/othello/woc2003.html#sf11 (view source for transcript)
    @Test
    public void seeleyVsHohne() {
        testKnowGame("F5F6E6F4G6C5G4G5H4H3F3G3E3E2E1F2F1H5H6H7D6D3C6F7E7E8C4C3D2C1D1G1C2D7G2B1D8C8G8F8B8B2A2A1B3A3B4C7B5A6B6A7B7A4A5A8H2H1G7H8",
                Player.O,
                46,
                18);
    }

    // This is a tie !
    // http://emmettnicholas.com/othello/woc2003.html#sf11 (view source for transcript)
    @Test
    public void seeleyVsHohne2() {
        testKnowGame("F5F6E6F4E3C5C4E7C6E2F3D3C3D2E1C1F2C2D1F1G6D6F7G5H5B5C7D7E8B6C8B3G3H3G4H4H2H6H7D8B4A5G7G2A6F8H1G1A4A3A2B2B7A8B8A7G8H8B1A1",
                null,
                32,
                32);
    }

    // http://emmettnicholas.com/othello/woc2003.html#sf11 (view source for transcript)
    @Test
    public void seeleyVsHohne3() {
        testKnowGame("F5F6E6F4G6C5G4G5H4H3G3H5D6F2H6H7E3F3C4D3C6E7D2C2F7F8E2E1G1F1D1C1B1C3B4D7D8C8E8C7B6B5A6A5A4B3G2H1H2A3A2A1B2A7B7A8H8G8G7B8",
                Player.X,
                33,
                31);
    }

    // http://emmettnicholas.com/othello/woc2003.html#sf11 (view source for transcript)
    @Test
    public void gotoVsMakoto() {
        testKnowGame("F5D6C3D3C4F4C5B4C6E6F6B3E3B5F3G4D2C2G3D1H3C7B6A5A6D7A4A3A2F7E2F1C1E1B2H4G5E7H6H5F2A1B1B7A8A7B8C8G6G7D8F8H8E8G1H1G8G2H2H7",
                Player.O,
                46,
                18);
    }

    // http://emmettnicholas.com/othello/woc2003.html#sf11 (view source for transcript)
    @Test
    public void gotoVsMakoto2() {
        testKnowGame("F5F6E6F4E3C5G6F3G5G3D3E2C4C3B5D6C6D7E7G4D2B6F7F2E1F1C2C1G1E8H3H4H6H2B4A3H5H7D1H1G2B3A4A5A6A7C8F8C7D8B7A8G8G7A2A1H8B2B8B1",
                Player.X,
                33,
                31);
    }

    // http://emmettnicholas.com/othello/woc2003.html#sf11 (view source for transcript)
    @Test
    public void hohneVsGoto() {
        testKnowGame("F5F6E6F4G6C5G4G5H4H3F3G3E3E2E1F2F1H5D6C7D3C2H6H7C4F7E7F8B5D7C6B6C8A5E8B3C3B4C1B2A1D8G8B7A2D2D1A3A4G7H8G2H2H1G1B1B8A8A7A6",
                Player.X,
                40,
                24);
    }

    @Test
    public void suekuniVsSeeley() {
        testKnowGame("F5F6E6F4E3C5G5G3G6F3F2E2D6G4C4C6B5C3B4A6D3B6A5E7F7C7E1D7F8D2B3A3A7H5C8H6H3H4H7D1F1E8D8C2H2A4G2A8B2A2C1B7B8H1G1H8G8G7B1A1",
                Player.O,
                42,
                22);
    }

    @Test
    public void suekuniVsSeeley2() {
        testKnowGame("F5D6C3D3C4F4F6F3E6E7D7G6F8F7G5H6H4B5B4E8D8C5C6C8B8C7A5G7B6A3G3D2A4B3F2H5E3G4H3F1G2A6C2D1E1E2G1B7A8H1A7B2H7H8G8H2A2A1B1C1",
                Player.X,
                35,
                29);

    }
}
